lighttpd (1.4.56~rc7-0+exp2) experimental; urgency=medium

  This version changes the way binary packages are laid out. lighttpd-1.4.56
  provides multiple modules for TLS. Therefore mod_openssl got moved out of
  the main lighttpd package, along with the dependency on openssl libraries.
  A new package was created for each TLS module.
   * lighttpd-mod-openssl
   * lighttpd-mod-mbedtls
   * lighttpd-mod-wolfssl
   * lighttpd-mod-nss
  If you use Recommends, lighttpd-mod-openssl will be installed automatically
  in bullseye to ease upgrading. After that, the recommendation will be
  dropped.

  The compression module was renamed from mod_compress to mod_deflate.
  Compression library dependencies have been moved out of the main lighttpd
  package and into a new package lighttpd-mod-deflate. If you use Recommends,
  lighttpd-mod-deflate will be installed automatically in bullseye to ease
  upgrading. In any case, the new default configuration cannot enable
  mod_deflate due to the split. To keep the module active you should run:

      lighty-enable-mod deflate

  Modules are coalesced to reduce the binary package count and runtime
  dependencies.
   * mod_authn_dbi (new) -> lighttpd-modules-dbi
   * mod_vhostdb_dbi -> lighttpd-modules-dbi
   * mod_magnet -> lighttpd-modules-lua
   * mod_cml -> lighttpd-modules-lua

  Do not depend on lighttpd-modules-* or lighttpd for using specific modules.
  Do depend on virtual packages lighttpd-mod-* instead.

 -- Glenn Strauss <gstrauss@gluelogic.com>  Mon, 26 Oct 2020 12:50:09 +0000

lighttpd (1.4.52-4) unstable; urgency=medium

  If mod_cgi is enabled, an alias is now configured:
    alias.url += ( "/cgi-bin/" => "/usr/lib/cgi-bin/" )
  whereas an alias was previously not specified.  "/usr/lib/cgi-bin" is the
  Debian standard location for CGI.  If an existing installation placed
  cgi-bin/ in the document tree, then the alias.url directive in 10-cgi.conf
  will need to be commented out, or the cgi-bin/ moved to /usr/lib/cgi-bin/

  For consistency and security, this version also changes lighttpd.conf
  to default to strict parsing and normalization of request URLs.
  Most websites will be unaffected by these more secure defaults.
  Some sites may need to comment out or to disable some of the
  strict options in server.http-parseopts, e.g. if a site encodes
  URLs in the url-path and requires "%2F" to be preserved as "%2F"
  instead of being decoded to "/".

 -- Glenn Strauss <gstrauss@gluelogic.com>  Sun, 13 Jan 2019 15:58:07 +0000

lighttpd (1.4.52-2+exp1) experimental; urgency=medium

  This version changes the way binary packages are laid out. Modules are
  coalesced to reduce the binary package count and runtime dependencies.
  Therefore two modules got moved out of the main lighttpd package:
   * mod_vhostdb_ldap -> lighttpd-modules-ldap
   * mod_vhostdb_mysql -> lighttpd-modules-mysql
  If you use Recommends, these packages will be installed automatically in
  buster to ease upgrading. After that, the recommendation will be dropped.

  Do not depend on lighttpd-modules-* or lighttpd for using specific modules.
  Do depend on virtual packages lighttpd-mod-* instead.

 -- Helmut Grohne <helmut.grohne@intenta.de>  Fri, 04 Jan 2019 08:23:03 +0100

lighttpd (1.4.31-4) unstable; urgency=high

  The default Debian configuration file for PHP invoked from FastCGI was
  vulnerable to local symlink attacks and race conditions when an attacker
  manages to control the PHP socket file (/tmp/php.socket up to 1.4.31-3)
  before the web server started. Possibly the web server could have been
  tricked to use a forged PHP.

  The problem lies in the configuration, thus this update will fix the problem
  only if you did not modify the file /etc/lighttpd/conf-available/15-fastcgi-php.conf
   If you did, dpkg will not overwrite your changes. Please make sure to set

        "socket" => "/var/run/lighttpd/php.socket"

  yourself in that case.

 -- Arno Töll <arno@debian.org>  Thu, 14 Mar 2013 01:57:42 +0100

lighttpd (1.4.30-1) unstable; urgency=medium

  This releases includes an option to force Lighttpd to honor the cipher order
  in ssl.cipher-list. This mitigates the effects of a SSL CBC attack commonly
  referred to as "BEAST attack". See [1] and CVE-2011-3389 for more details.

  To minimze the risk of this attack it is recommended either to disable all CBC
  ciphers (beware: this will break reasonably old clients or those who support
  CBC ciphers only), or pursue clients to use safe ciphers where possible at
  least. To do so, set

  ssl.cipher-list =  "ECDHE-RSA-AES256-SHA384:AES256-SHA256:RC4:HIGH:!MD5:!aNULL:!EDH:!AESGCM"
  ssl.honor-cipher-order = "enable"

  in your /etc/lighttpd/conf-available/10-ssl.conf file or on any SSL enabled
  host you configured. If you did not change this file previously, this upgrade
  will update it automatically.

  [1] http://blog.ivanristic.com/2011/10/mitigating-the-beast-attack-on-tls.html

 -- Arno Töll <debian@toell.net>  Sun, 18 Dec 2011 20:26:50 +0100

lighttpd (1.4.23-1) unstable; urgency=low

  spawn-fcgi is now separate package. Please install "spawn-fcgi" package if 
  you need it.

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Thu, 09 Jul 2009 15:53:14 +0200
